FROM debian:jessie

RUN apt-get update && apt-get install -y build-essential python2.7 python-dev python-pip screen

RUN useradd www

RUN mkdir -p /var/www/fsioct
WORKDIR /var/www/fsioct
ADD requirements.txt requirements.txt
ADD server.py server.py
ADD app app

RUN pip install -r requirements.txt

EXPOSE 5000
