import os

from flask import Flask
from flask.ext.socketio import SocketIO
from celery import Celery

#from app.api import create_api
#from app.frontend import create_frontend
from app.sockets import create_sockets
from app.tasks import create_tasks

flask_app = Flask(__name__)
flask_app.config.from_pyfile('config.py', silent=True)
flask_app.debug = True
socket_app = SocketIO(flask_app)
celery_app = Celery(flask_app.name, broker=flask_app.config['CELERY_BROKER_URL'])

#create_api(flask_app)
#create_frontend(flask_app)
create_sockets(socket_app)
create_tasks(celery_app)

if __name__ == '__main__':
    socket_app.run(flask_app)
