from datetime import timedelta

CELERY_BROKER_URL = 'redis://redis:6379/0'
#CELERY_RESULT_BACKEND = 'rpc://'

'''
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TIMEZONE = 'Europe/Stockholm'
CELERY_ENABLE_UTC = True
'''

"""
CELERYBEAT_SCHEDULE = {
    'weather': {
        'task': 'weather.fetch_all',
        'schedule': timedelta(minutes=1)
    }
}
"""

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://wwmo:wwmo@mysql/wwmo'
