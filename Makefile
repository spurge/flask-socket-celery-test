dir = $(shell pwd)
define screen
screen bash
screen -t celery su www -c "celery -A server.celery_app worker --autoreload"
split -v
focus right
screen -t flask su www -c "python server.py"
endef
export screen

default: dev

build:
	docker build -t fsioct-app --rm .

redis:
	docker run --name fsioct-redis -d redis || docker start fsioct-redis

dev: redis build
	docker run --name fsioct-app -it -p 127.0.0.1:5000:5000 -e TERM=xterm \
		--link fsioct-redis:redis \
		-v $(dir)/app:/var/www/fsioct/app fsioct-app \
		sh -c "/bin/echo '$$screen' > screenrc && /usr/bin/screen -c screenrc" || \
		docker start -i fsioct-app

stop:
	docker stop fsioct-app fsioct-redis

clean:
	docker rm fsioct-app fsioct-redis
